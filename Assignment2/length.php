

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Length Convertor</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="container " style="text-align: center;" >
<img class="pb-3" src="https://img.icons8.com/cute-clipart/50/000000/ruler.png">
<span style="font-size: 35px; color: blue; font-weight: 800;">Length Convertor</span>
</div>
<div class="container mt-5">
    <form method="post">
      <div class="form-group">
        <label>Length in Meters</label>
        <input type="number" class="form-control" name="meter">
         </div>
      <div class="form-group">
        <label>Length in Yards</label>
        <input type="number" class="form-control" name="yard">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Convert</button>
    </form>
</div>
    
    <?php

if(isset($_POST['submit'])){
    
        $meter=$_POST['meter'];
        $yard = $_POST['yard'];
        
        if($meter != NULL){
            $converted = $meter . " meter is ". $meter * 1.09361 . " yards";
            //echo $converted;
        }
        elseif($yard != NULL){
            $converted = $yard ." yard is ". $yard * 0.999996984 . " meters";
            //   echo $converted;
        }
    }
    ?>

    <div class="container mt-5" style="background-color: #0069D9">
        
        <span style="font-size: 25px"> <?php if(isset($_POST['submit'])){echo $converted;} ?> </span>
        
    </div>
    



    <div class="container pt-4">
    <div class="row pt-5">
        
            <div class="col-6">
                <a href="/Assignment2/weight.php">
                <img src="https://img.icons8.com/cotton/100/000000/weight-care.png">
                <p>
                    Convert Weight
                    </p>
                </a>
            </div>
    
        <div class="col-6">
            <a href="/Assignment2/temp.php">
            <img src="https://img.icons8.com/ultraviolet/100/000000/temperature.png">
            <p>

                Convert Temperature
                </p>
        </a>
        </div>
    
    </div>
</div>
</body>
</html>
