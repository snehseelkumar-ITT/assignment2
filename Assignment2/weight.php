<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Length Convertor</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="container " style="text-align: center;" >
<img class="pb-3" src="https://img.icons8.com/cotton/50/000000/weight-care.png">
<span style="font-size: 35px; color: blue; font-weight: 800;">Weight Convertor</span>
</div>
<div class="container mt-5">
    <form method="post">
      <div class="form-group">
        <label>Weight in Kilos</label>
        <input type="number" class="form-control" name="kilo">
         </div>
      <div class="form-group">
        <label>Weight in Pound</label>
        <input type="number" class="form-control" name="pound">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Convert</button>
    </form>
</div>
    
    <?php

if(isset($_POST['submit'])){
    
        $kilo=$_POST['kilo'];
        $pound = $_POST['pound'];
        
        if($kilo != NULL){
            $converted = $kilo . " kilo is ". $kilo * 2.20462 . " pound";
            //echo $converted;
        }
        elseif($pound != NULL){
            $converted = $pound ." pound is ". $pound * 0.453592 . " kilo";
            //   echo $converted;
        }
    }
    ?>

    <div class="container mt-5" style="background-color: #0069D9">
        
        <span style="font-size: 25px"> <?php if(isset($_POST['submit'])){echo $converted;} ?> </span>
        
    </div>
    



    <div class="container pt-4">
    <div class="row pt-5">
        
            <div class="col-6">
            <a href="/Assignment2/length.php">
                <img src="https://img.icons8.com/cute-clipart/100/000000/ruler.png">
               <p>

                   Convert Length
               </p>
                </a>
            </div>
    
        <div class="col-6">
            <a href="/Assignment2/temp.php">
            <img src="https://img.icons8.com/ultraviolet/100/000000/temperature.png">
            <p>

                Convert Temperature
            </p>
        </a>
        </div>
    
    </div>
</div>
</body>
</html>
