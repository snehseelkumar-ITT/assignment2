<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Temperature Convertor</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="container " style="text-align: center;" >
<img class="pb-3" src="https://img.icons8.com/ultraviolet/50/000000/temperature.png">
<span style="font-size: 35px; color: blue; font-weight: 800;">Temperature Convertor</span>
</div>
<div class="container mt-5">
    <form method="post">
      <div class="form-group">
        <label>Temperature in Celsius</label>
        <input type="number" class="form-control" name="cel">
         </div>
      <div class="form-group">
        <label>Temperature in Farhenheit</label>
        <input type="number" class="form-control" name="far">
      </div>
      <button type="submit" name="submit" class="btn btn-primary">Convert</button>
    </form>
</div>
    
    <?php

if(isset($_POST['submit'])){
    
        $cel=$_POST['cel'];
        $far = $_POST['far'];
        
        if($cel != NULL){
            $converted = $cel . " celsius is ". (($cel * 1.8) + 32) . " Farhenheit";
            //echo $converted;
        }
        elseif($far != NULL){
            $converted = $far ." farenheit is ". (5*($far - 32))/9 . " Celsius";
            //   echo $converted;
        }
    }
    ?>

    <div class="container mt-5" style="background-color: #0069D9">
        
        <span style="font-size: 25px"> <?php if(isset($_POST['submit'])){echo $converted;} ?> </span>
        
    </div>
    



    <div class="container pt-4">
    <div class="row pt-5">
        
            <div class="col-6">
            <a href="/Assignment2/length.php">
                <img src="https://img.icons8.com/cute-clipart/100/000000/ruler.png">
              <p>

                  Convert Length
              </p>
                </a>
            </div>
    
            <div class="col-6">
                <a href="/Assignment2/weight.php">
                <img src="https://img.icons8.com/cotton/100/000000/weight-care.png">
                <p>
                    Convert Weight
                </p>
                </a>
            </div>
    
    
    </div>
</div>
</body>
</html>
